#coding:utf-8
from django.db import models
from django.utils import timezone
# Create your models here.

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=200)
    slug= models.CharField(max_length=200)
    body= models.TextField ()
    pub_date = models.DateTimeField (default=timezone.now)

    class Meta:
          ordering =('-pub_date',)

    def __str__(self):
          return self.title

#鉴权mode
class Permission(models.Model):
    class Meta:
        #权限信息，这里定义的权限的名字，后面是描述信息，描述信息是在django admin中显示权限用的
        permissions = (
            ('views_slg_users_tem', '查看玩家管理'),
            ('views_slg_alliance_tem', '查看联盟管理'),
            ('views_slg_mail_notice_tem', '查看公告邮件'),
            ('views_slg_order_tem', '查看订单系统'),
            ('views_slg_reward_tem', '查看礼包奖励'),
            ('views_slg_service_reply_tem', '查看客服反馈'),
            ('views_slg_user_log_tem', '查看玩家日志'),
            ('views_slg_server_tem', '查看服务器管理'),
            ('views_slg_manager_tem', '查看管理员管理'),
        )

#搜索接口样例mode---haystack no
class ArticlePost(models.Model):

    #author = models.ForeignKey(User, related_name='article')     #考究
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=500)
    #column = models.ForeignKey(ArticleColumn, related_name='article_column')
    body = models.TextField()
    created = models.DateTimeField(default=timezone.now())
    updated = models.DateTimeField(auto_now_add=True)
    # media/%Y%m%d/为图片的真实放置路径，因为settings中已经配置了MEDIA_ROOT为media文件夹
    avatar = models.ImageField(upload_to='%Y%m%d/', blank=True)

from django.db import models

class User(models.Model):
	username = models.CharField(verbose_name='用户名', max_length=40, unique=True)
	password = models.CharField(verbose_name='口令', max_length=256)

	def __str__(self):
		return self.username
