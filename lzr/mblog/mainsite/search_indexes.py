__author__ = 'YuLongZ'
#coding:utf-8
import requests,json
# import datetime
# from haystack import indexes
# from .models import ArticlePost

# class ArticlePostIndex(indexes.SearchIndex, indexes.Indexable):  # 类名必须为需要检索的Model_name+Index，这里需要检索ArticlePost，所以创建ArticlePostIndex类
#     text = indexes.CharField(document=True, use_template=True)  # 创建一个text字段
#     # author = indexes.CharField(model_attr='author')  # 创建一个author字段,model_attr='author'代表对应数据模型ArticlePost中的author字段，可以删
#     # title = indexes.CharField(model_attr='title')  # 创建一个title字段
#     # body = indexes.CharField(model_attr='body')
#
#     # 对那张表进行查询
#     def get_model(self):  # 重载get_model方法，必须要有！
#         # 返回这个model
#         return ArticlePost
#
#     # 针对哪些数据进行查询---这不是只对数据库查询吗？
#     def index_queryset(self, using=None):  # 重载index_..函数
#         """Used when the entire index for model is updated."""
#         return self.get_model().objects.filter(updated__lte=datetime.datetime.now())
#         #return self.get_model().objects.all()
# match，match_all,match_phrase,term,bool(),mulitmatch,dis_max

def match(query,fields):
    query_from = {
      "query": {
          "match": {
              fields:query
          }
      }
    }
    return json.dumps(query_from)

def match_all(size,from_,source,sort,script):
# "script_fields"{ #脚本查询，对查询到的数据可以做相对应的预处理再显示出来
#  "new_field":{
#  "script":{
#   "lang":"painless",
#   "source":"doc['order_date'].value + 'hello'"
# }
# }
# }
    query_form = {
        "query": {
            "match_all":{}
        },
        "size":  size,
        "from": from_,
        "source": source,
        "sort":sort,
        "script_fields":script
    }
    return json.dumps(query_form)

def match_phrase(field,phrase):
    query_form = {
        "query":{
            "match_phrase":{
                field:phrase
            }
        }
    }
    return json.dumps(query_form)



def multi_match(query,fields=[]):
    query_from = {
      "query": {
          "multi_match": {
              "query": query,
              "fields": fields
          }
      }
    }
    return json.dumps(query_from)

#使用search接口进行搜索
def do_search(fields,value):
    timeout = (5,5)
    url = 'http://192.168.200.232:9200/lzr/_search'
    query = match(value,fields)
    headers={"Content-Type":"application/json","Accept":"application/json"}
    try:
      resp = requests.get(url,headers=headers,data=query,timeout=timeout)
    except requests.ConnectionError as e:
      return "搜索引擎链接超时，请稍后重试"
    source = json.loads(resp.text)["hits"]["hits"][0]["_source"]

    return source