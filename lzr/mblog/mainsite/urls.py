__author__ = 'YuLongZ'
#coding:utf-8
from django.conf.urls import url
from .views import search
#from haystack.views import SearchView


app_name='mainstite'
urlpatterns=[
    # SearchView()视图函数，默认使用的HTML模板路径为templates/search/search.html
    url(r'search/$',search, name='elastic_search'),
]