#coding:utf-8
from django.shortcuts import render
from django.http import  HttpResponse
from django.shortcuts import redirect
from django.template.loader import get_template
from datetime import datetime
from .models import Post,User
from .search_indexes import do_search
from functools import wraps
import json
# Create your views here.

def check_login(func):
    @wraps(func)     #是wraps一个可以确保调用装饰器时不会改变原函数属性
    def inner(request, *arg, **kwargs):
        if request.session.get('is_login', None):
            return func(request, *arg, **kwargs)
        return redirect('/login/')
    return inner

@check_login
def homepage_2(request):
    template = get_template("index.html")
    posts = Post.objects.all()
    now = datetime.now()
    user = request.session['username']
    html = template.render(locals())
    return HttpResponse(html)

def showpost(request,slug):
    request=request
    slug=slug
    template = get_template("post.html")
    try:
        post = Post.objects.get(slug=slug)
        if post != None:
            html = template.render(locals())
            return HttpResponse(html)
    except:
        return redirect('/')

def search(request):
    request.encoding='utf-8'
    if 'q' in request.GET and request.GET['q']:
        message=do_search('title',request.GET['q'])
        print(type(message),message)
        message.setdefault('user',request.session['username'])
    else:
        #message = 'normal falid'
        return render(request,'index.html',{'mess':"不允许传空"})
    return render(request,'search.html',message)

def login(request):
    if request.session.get('is_login', None):  # 检查是否已登录
        return redirect('/') #跳转到主页
    if request.method == 'POST':  # 如果有提交表单
        username = request.POST.get('username', '').strip()
        password = request.POST.get('password', '').strip()
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return render(request, 'login.html', {'mess': '用户不存在'})
        else:
            if password == user.password:
                request.session['is_login'] = True  # 标记已登录
                request.session['user_id'] = user.id
                request.session['username'] = username
                return redirect('/')
            else:
                return render(request, 'login.html', {'mess': '密码输入错误'})
    return render(request, 'login.html')

def logout(request):
    if request.session.get('is_login', None):
        request.session.flush()
    return redirect('/login/')


def register(request):
    if request.session.get('is_login', None):
        request.session.flush()
    if request.method == 'POST':
        username = request.POST.get('username', '').strip()
        password = request.POST.get('password', '').strip()
        re_password = request.POST.get('re_password', '').strip()
        if password == re_password:
            try:
                User.objects.get(username=username)
            except User.DoesNotExist:
                new_user = User(username=username, password=password)
                new_user.save()
                return render(request, 'register.html', {'mess': '用户注册成功'})
            else:
                return render(request, 'register.html', {'mess': '该用户已被注册'})
        else:
            return render(request, 'register.html', {'mess': '两次密码输入不一致'})
    return render(request, 'register.html')

def demo(request):
    fa = {"1":1,"2":2}
    return HttpResponse(str(fa))