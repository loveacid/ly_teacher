from django.contrib import admin
from .models import Post,Permission,User
# Register your models here.
class Postadmin(admin.ModelAdmin):
    list_display = ('title','slug','pub_date')
admin.site.register(Post,Postadmin)
admin.site.register(User)
