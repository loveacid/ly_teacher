/**
 * Created by YuLongZ on 2020/4/30.
 */
function changePermission() {
    var permissionList = $('input[name="permissionList"]:checked');
    var permissions = '';
    $.each(permissionList, function (index, value, array) {
        if (index+1 == permissionList.length) {   // 最后一位，不加逗号
            permissions += permissionList[index].value;
        } else {
            permissions += permissionList[index].value + ', ';
        }
    });
    $.ajax({
        url: '/changePermission',
        type: 'POST',
        data: {
            username: $('#usernamePermission').val(),
            permissions: permissions
        },
        success: function (data, textStatus) {
            if (data == 1) {
                alert('修改成功！');
                window.location.href = 'index';

            } else if (data == -1) {
                alert('未知错误！');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    })
}
